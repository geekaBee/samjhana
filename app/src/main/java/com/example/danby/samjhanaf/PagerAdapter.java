package com.example.danby.samjhanaf;

/**
 * Created by aayush on 6/6/16.
 */
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

public class PagerAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;

    public PagerAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                Morning tab1 = new Morning();
                return tab1;
            case 1:
                Afternoon tab2 = new Afternoon();
                return tab2;
            case 2:
                Evening tab3 = new Evening();
                return tab3;
            case 3:
                Night tab4 = new Night();
                return tab4;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}