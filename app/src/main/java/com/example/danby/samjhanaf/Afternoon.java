package com.example.danby.samjhanaf;



import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

/**
 * Created by Danby on 5/25/2016.
 */
public class Afternoon extends Fragment{

    ListView l;
    String[] afternoon;
    String[] time;
    int[] images={R.drawable.cap4,R.drawable.cap2,R.drawable.cap3};

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.afternoon,container,false);

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Resources res = getResources();
        afternoon = res.getStringArray(R.array.Afternoon);
        time = res.getStringArray(R.array.time);
        l = (ListView) getView().findViewById(R.id.afternoonList);
        customAdapter1 adapter = new customAdapter1(getActivity(),afternoon,images,time);
        l.setAdapter(adapter);
    }

}

class customAdapter1 extends ArrayAdapter<String>
{
    Context context;
    String[] afternoonArray;
    String[] timeArray;

    int[] images;
    customAdapter1(Context c,String[] morning,int imgs[],String[] time){
        super(c,R.layout.single_row,R.id.textView,morning);
        this.context=c;
        this.images=imgs;
        this.afternoonArray=morning;
        this.timeArray=time;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View row = inflater.inflate(R.layout.single_row,parent,false);
        ImageView myImage = (ImageView) row.findViewById(R.id.imageView);
        TextView medicineName = (TextView) row.findViewById(R.id.textView);
        TextView time = (TextView) row.findViewById(R.id.textView1);

        myImage.setImageResource(images[position]);
        medicineName.setText(afternoonArray[position]);
        time.setText(timeArray[position]);
        return row;

    }
}
