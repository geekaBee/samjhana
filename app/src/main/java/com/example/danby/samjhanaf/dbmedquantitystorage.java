package com.example.danby.samjhanaf;

import com.orm.SugarRecord;

/**
 * Created by aayush on 6/7/16.
 */
public class dbmedquantitystorage extends SugarRecord {

    long med_id;
    int amount;

    public dbmedquantitystorage() {

    }

    public dbmedquantitystorage(long m, int a) {

        this.med_id= m;
        this.amount =a;

    }

    public void setMed_ID(long m) {
        this.med_id= m;
    }
    public long getMed_ID(){

        return med_id;
    }


}


