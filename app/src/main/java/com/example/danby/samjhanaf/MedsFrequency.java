package com.example.danby.samjhanaf;


import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Checked;

import java.util.Calendar;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class MedsFrequency extends Fragment implements Validator.ValidationListener {

    public MedsFrequency() {
        // Required empty public constructor
    }
    public RadioButton fbtn1;
    public RadioButton sdbtn1;
    public Button freqbtn;
    //checks for radiobutton validation
    @Checked(message = "Choose medicine frequency")
    public RadioGroup freq;
    //checks for radiobutton validation
    @Checked(message = "Choose Day")
    public RadioGroup daysel;
    public String medsfre;
    public String dayfre;
    Validator validator;
    long med_id;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v=inflater.inflate(R.layout.fragment_meds_frequency, container, false);
        validator = new Validator(this);
        validator.setValidationListener(this);

        freqbtn=(Button)v.findViewById(R.id.frequencybtn);
        freq=(RadioGroup)v.findViewById(R.id.frequency);
        daysel=(RadioGroup)v.findViewById(R.id.selectday);
        Bundle meddata = getArguments();
        med_id=meddata.getLong("medicationId");
        Log.e("medId it is", Long.toString(med_id));

        //Next button event for frequency fragment
        freqbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int selectedfreq = freq.getCheckedRadioButtonId();
                int selectedday = daysel.getCheckedRadioButtonId();

                fbtn1=(RadioButton) getView().findViewById(selectedfreq);
                sdbtn1=(RadioButton) getView().findViewById(selectedday);
                //calls validator
                validator.validate();
            }
        });


        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    //calls when validation succed
    @Override
    public void onValidationSucceeded() {
        Toast.makeText(getActivity(), "Add dosage and time", Toast.LENGTH_SHORT).show();
        medsfre=fbtn1.getText().toString();//radio button value for frequency choosen
        dayfre=sdbtn1.getText().toString();//radion button value for day choosen

        //calls time and dosage fragment based on the radio button value for frequency
        Bundle data=new Bundle();
        data.putLong("medicationId",med_id);

        FragmentManager manager = getFragmentManager();
        if(medsfre.equals("1 time a day")){
            onetime one=new onetime();
            one.respond(dayfre);
            one.setArguments(data);
            manager.beginTransaction().replace(R.id.group,one).addToBackStack(null)
                    .commit();
        }
        else if (medsfre.equals("2 times a day")){
            twotime two=new twotime();
            two.respond(dayfre);
            two.setArguments(data);
            manager.beginTransaction().replace(R.id.group,two).addToBackStack(null).commit();
        }
        else if (medsfre.equals("3 times a day")){
            threetime three=new threetime();
            three.respond(dayfre);
            three.setArguments(data);
            manager.beginTransaction().replace(R.id.group,three).addToBackStack(null).commit();
        }
        else if (medsfre.equals("4 times a day")){
            fourtime four=new fourtime();
            four.respond(dayfre);
            four.setArguments(data);
            manager.beginTransaction().replace(R.id.group,four).addToBackStack(null).commit();
        }
        else{
            System.out.print("test");
        }
    }


    //calls when validation fails
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(getActivity());

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
               // Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
                Snackbar snackbar=Snackbar.make(view, message, Snackbar.LENGTH_LONG);
                snackbar.show();

            }
        }
    }


}
