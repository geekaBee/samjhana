package com.example.danby.samjhanaf;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by Danby on 5/6/2016.
 */
public class AlarmReceiver extends BroadcastReceiver{

    @Override
        public void onReceive(Context context, Intent intent) {
            String get = intent.getExtras().getString("extra");
            Long med_id = intent.getExtras().getLong("medId");
            Log.e("id in alarm receiver",Long.toString(med_id));
            Toast.makeText(context, "Alarm time has been reached", Toast.LENGTH_LONG).show();
            Intent service_intent = new Intent(context, RingtonePlaying.class);
            service_intent.putExtra("extra",get);
            service_intent.putExtra("medId",med_id);
            context.startService(service_intent);
        }
    }
