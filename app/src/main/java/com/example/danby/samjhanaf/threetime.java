package com.example.danby.samjhanaf;


import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Min;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.util.Calendar;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class threetime extends Fragment implements Validator.ValidationListener,Communicator{

    public Button timesel;
    public Button timesel2;
    public Button timesel3;
    public Button btnextsel;
    @NotEmpty(message = "Please select a time")
    public TextView timesfld;
    @NotEmpty(message = "Please select a time")
    public TextView timesfld2;
    @NotEmpty(message = "Please select a time")
    public TextView timesfld3;
    public String times1;
    public String times2;
    public String times3;
    public int dosage;
    @Min(value = 1,message = "Please select appropriate dosage")
    public TextView ldosage;
    public Button incbtn;
    public Button decbtn;
    public int dosage2;
    @Min(value = 1,message = "Please select appropriate dosage")
    public TextView ldosage2;
    public Button incbtn2;
    public Button decbtn2;
    public int dosage3;
    @Min(value = 1,message = "Please select appropriate dosage")
    public TextView ldosage3;
    public Button incbtn3;
    public Button decbtn3;
    Validator validator;

    Intent intent;
    Calendar c1 = Calendar.getInstance();
    Calendar c2 = Calendar.getInstance();
    Calendar c3 = Calendar.getInstance();
    PendingIntent pendingIntent;
    AlarmManager alarmManager;
    String daysFrequency;

    public threetime() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v= inflater.inflate(R.layout.fragment_threetime, container, false);
        validator = new Validator(this);
        validator.setValidationListener(this);

        timesfld = (TextView)v.findViewById(R.id.time1val);
        timesfld2 = (TextView)v.findViewById(R.id.time2val);
        timesfld3 = (TextView)v.findViewById(R.id.time3val);


        timesel=(Button)v.findViewById(R.id.selecttime1);//time select button
        timesel2=(Button)v.findViewById(R.id.selecttime2);//time select button
        timesel3=(Button)v.findViewById(R.id.selecttime3);//time select button

        btnextsel=(Button)v.findViewById(R.id.btndtimenxt);

        ldosage=(TextView)v.findViewById(R.id.currentdosage);
        dosage = Integer.valueOf(ldosage.getText().toString());
        incbtn=(Button)v.findViewById(R.id.incbtn);//dosage increase button
        decbtn=(Button)v.findViewById(R.id.decbtn);//decrease dosage button

        ldosage2=(TextView)v.findViewById(R.id.currentdosage2);
        dosage2 = Integer.valueOf(ldosage2.getText().toString());
        incbtn2=(Button)v.findViewById(R.id.incbtn2);//dosage increase button
        decbtn2=(Button)v.findViewById(R.id.decbtn2);//decrease dosage button

        ldosage3=(TextView)v.findViewById(R.id.currentdosage3);
        dosage3 = Integer.valueOf(ldosage3.getText().toString());
        incbtn3=(Button)v.findViewById(R.id.incbtn3);//dosage increase button
        decbtn3=(Button)v.findViewById(R.id.decbtn3);//decrease dosage button

        intent = new Intent(getActivity(), AlarmReceiver.class);
        intent.putExtra("extra", "on");
        alarmManager = (AlarmManager)(getActivity().getSystemService( Context.ALARM_SERVICE ));


        timesel.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                DialogFragment newFragment = new com.example.danby.samjhanaf.TimePicker();
                newFragment.setTargetFragment(threetime.this,0);
                newFragment.show(getActivity().getSupportFragmentManager(), "timePicker");

            }
        });
        timesel2.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {
                DialogFragment newFragment = new com.example.danby.samjhanaf.TimePicker2();
                newFragment.setTargetFragment(threetime.this,0);
                newFragment.show(getActivity().getSupportFragmentManager(), "timePicker2");

            }
        });
        timesel3.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {
                DialogFragment newFragment = new com.example.danby.samjhanaf.TimePicker3();
                newFragment.setTargetFragment(threetime.this,0);
                newFragment.show(getActivity().getSupportFragmentManager(), "timePicker3");

            }
        });




        incbtn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

                if (dosage >= 0) {
                    dosage++;
                }
                else{
                    dosage=0;
                }

                ldosage.setText(Integer.toString(dosage));
            }
        });
        decbtn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

                if (dosage > 0) {
                    dosage--;
                } else {
                    dosage = 0;
                }

                ldosage.setText(Integer.toString(dosage));
            }
        });
        incbtn2.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

                if (dosage2 >= 0) {
                    dosage2++;
                }
                else{
                    dosage2=0;
                }

                ldosage2.setText(Integer.toString(dosage2));
            }
        });
        decbtn2.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

                if (dosage2 > 0) {
                    dosage2--;
                } else {
                    dosage2 = 0;
                }

                ldosage2.setText(Integer.toString(dosage2));
            }
        });
        incbtn3.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

                if (dosage3 >= 0) {
                    dosage3++;
                }
                else{
                    dosage3=0;
                }

                ldosage3.setText(Integer.toString(dosage3));
            }
        });
        decbtn3.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

                if (dosage3 > 0) {
                    dosage3--;
                } else {
                    dosage3 = 0;
                }

                ldosage3.setText(Integer.toString(dosage3));
            }
        });
        btnextsel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validator.validate();

            }
        });
        return v;
    }

    public void respond(String text){
        daysFrequency=text;
        Log.e("Mes", daysFrequency);
    }

    public void respond1(Calendar c){
        c1=c;
        int hour=c1.get(Calendar.HOUR_OF_DAY);
        int minute=c1.get(Calendar.MINUTE);
        Log.e("Value of hour",String.valueOf(hour));
        Log.e("Value of minute",String.valueOf(minute));
    }

    public void respond2(Calendar c){
        c2=c;
        int hour=c2.get(Calendar.HOUR_OF_DAY);
        int minute=c2.get(Calendar.MINUTE);
        Log.e("Value of hour",String.valueOf(hour));
        Log.e("Value of minute",String.valueOf(minute));
    }

    public void respond3(Calendar c){
        c3=c;
        int hour=c3.get(Calendar.HOUR_OF_DAY);
        int minute=c3.get(Calendar.MINUTE);
        Log.e("Value of hour",String.valueOf(hour));
        Log.e("Value of minute",String.valueOf(minute));
    }

    public void respond4(Calendar c){

    }


    @Override
    public void onValidationSucceeded() {
        final int _id = (int) System.currentTimeMillis();
        if(daysFrequency.equals("Everyday")) {
            Log.e("We","are In");
            pendingIntent = PendingIntent.getBroadcast(getActivity(), _id + 111, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, c1.getTimeInMillis(), AlarmManager.INTERVAL_DAY, pendingIntent);

            pendingIntent = PendingIntent.getBroadcast(getActivity(), _id + 112, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, c2.getTimeInMillis(), AlarmManager.INTERVAL_DAY, pendingIntent);

            pendingIntent = PendingIntent.getBroadcast(getActivity(), _id + 113, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, c3.getTimeInMillis(), AlarmManager.INTERVAL_DAY, pendingIntent);
        }

        if(daysFrequency.equals("Every two days")) {
            pendingIntent = PendingIntent.getBroadcast(getActivity(), _id + 221, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, c1.getTimeInMillis(), 2*AlarmManager.INTERVAL_DAY, pendingIntent);

            pendingIntent = PendingIntent.getBroadcast(getActivity(), _id + 222, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, c2.getTimeInMillis(), 2*AlarmManager.INTERVAL_DAY, pendingIntent);

            pendingIntent = PendingIntent.getBroadcast(getActivity(), _id + 223, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, c3.getTimeInMillis(), AlarmManager.INTERVAL_DAY, pendingIntent);
        }

        else if(daysFrequency.equals("Every three days")) {
            pendingIntent = PendingIntent.getBroadcast(getActivity(), _id + 331, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, c1.getTimeInMillis(), 3*AlarmManager.INTERVAL_DAY, pendingIntent);

            pendingIntent = PendingIntent.getBroadcast(getActivity(), _id + 332, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, c2.getTimeInMillis(), 3*AlarmManager.INTERVAL_DAY, pendingIntent);

            pendingIntent = PendingIntent.getBroadcast(getActivity(), _id + 333, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, c3.getTimeInMillis(), AlarmManager.INTERVAL_DAY, pendingIntent);
        }


        else if(daysFrequency.equals("Every four days")) {
            pendingIntent = PendingIntent.getBroadcast(getActivity(), _id + 441, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, c1.getTimeInMillis(), 4*AlarmManager.INTERVAL_DAY, pendingIntent);

            pendingIntent = PendingIntent.getBroadcast(getActivity(), _id + 442, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, c2.getTimeInMillis(), 4*AlarmManager.INTERVAL_DAY, pendingIntent);

            pendingIntent = PendingIntent.getBroadcast(getActivity(), _id + 443, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, c3.getTimeInMillis(), AlarmManager.INTERVAL_DAY, pendingIntent);
        }



        Intent finish = new Intent(getActivity(), MainActivity.class);
        Toast.makeText(getActivity(), "Thank you", Toast.LENGTH_SHORT).show();
        startActivity(finish);
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(getActivity());

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Snackbar snackbar=Snackbar.make(view, message, Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }

    }
}
