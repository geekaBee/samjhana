
package com.example.danby.samjhanaf;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Min;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.util.Calendar;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class onetime extends Fragment implements Validator.ValidationListener,Communicator{


    public onetime() {
        // Required empty public constructor
    }

    Intent finish;
    public Button timesel;
    public Button btnextsel;
    @NotEmpty(message = "Please select a time")
    public TextView timesfld;
    public String times1;
    public int dosage;
    @Min(value = 1,message = "Please select appropriate dosage")
    public TextView ldosage;
    public Button incbtn;
    public Button decbtn;
    public String daysFrequency;
    Validator validator;
    Intent intent;
    Calendar c = Calendar.getInstance();
    PendingIntent pendingIntent;
    AlarmManager alarmManager;
    Long med_id;
    int hour;
    int minute;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v=inflater.inflate(R.layout.fragment_onetime, container, false);
        //for validation
        validator = new Validator(this);
        validator.setValidationListener(this);

        timesfld = (TextView)v.findViewById(R.id.time1val);
        ldosage=(TextView)v.findViewById(R.id.currentdosage);
        dosage = Integer.valueOf(ldosage.getText().toString());
        incbtn=(Button)v.findViewById(R.id.incbtn);//dosage increase button
        decbtn=(Button)v.findViewById(R.id.decbtn);//decrease dosage button
        timesel=(Button)v.findViewById(R.id.selecttime1);//time select button
        btnextsel=(Button)v.findViewById(R.id.btndtimenxt);

        Bundle meddata = getArguments();
        med_id=meddata.getLong("medicationId");
        Log.e("medId it is", Long.toString(med_id));



        intent = new Intent(getActivity(), AlarmReceiver.class);
        intent.putExtra("extra","on");
        intent.putExtra("medId",med_id);
        alarmManager = (AlarmManager)(getActivity().getSystemService( Context.ALARM_SERVICE ));

        //select time button event
        timesel.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {
                DialogFragment newFragment = new TimePicker();
                newFragment.setTargetFragment(onetime.this,0);
                newFragment.show(getActivity().getSupportFragmentManager(), "timePicker");

            }
        });
        //Next button event for single time chosen fragment
        btnextsel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validator.validate();

            }
        });


        //increase dosage button event
        incbtn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

                if (dosage >= 0) {
                    dosage++;
                }
                else{
                    dosage=0;
                }

                ldosage.setText(Integer.toString(dosage));
            }
        });

        //decrease dosage button event
        decbtn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

                if (dosage > 0) {
                    dosage--;
                } else {
                    dosage = 0;
                }

                ldosage.setText(Integer.toString(dosage));
            }
        });
        return v;
    }


    public void respond(String text){
        daysFrequency=text;
        Log.e("Mes",daysFrequency);
    }

    public void respond1(Calendar c1){
        c=c1;
        hour=c.get(Calendar.HOUR_OF_DAY);
        minute=c.get(Calendar.MINUTE);
        Log.e("Value of hour",String.valueOf(hour));
        Log.e("Value of minute",String.valueOf(minute));
    }
    public void respond2(Calendar c1){

    }
    public void respond3(Calendar c1){

    }
    public void respond4(Calendar c1){

    }
    //calls when validation succeed


    @Override
    public void onValidationSucceeded() {
        //getTargetFragment();
        //final int _id = (int) System.currentTimeMillis();
        int id = (int) (long) med_id;
       if(daysFrequency.equals("Everyday")) {
               Log.e("id received",Integer.toString(id));
               pendingIntent = PendingIntent.getBroadcast(getActivity(), id+1, intent, PendingIntent.FLAG_UPDATE_CURRENT);
               alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), AlarmManager.INTERVAL_DAY, pendingIntent);
        }

        if(daysFrequency.equals("Every two days")) {
           Log.e("2day","has been reached");
           pendingIntent = PendingIntent.getBroadcast(getActivity(), id+2, intent, PendingIntent.FLAG_UPDATE_CURRENT);

           alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), 2*AlarmManager.INTERVAL_DAY, pendingIntent);
        }
        else if(daysFrequency.equals("Every three days")) {
            pendingIntent = PendingIntent.getBroadcast(getActivity(),  id+3, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), 3*AlarmManager.INTERVAL_DAY, pendingIntent);
        }

        else if(daysFrequency.equals("Every four days")) {
            pendingIntent = PendingIntent.getBroadcast(getActivity(),  id+4, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), 4*AlarmManager.INTERVAL_DAY, pendingIntent);
        }

        finish = new Intent(getActivity(), MainActivity.class);
        times1= timesfld.getText().toString();//value for time set(in string)
        int dosval=Integer.parseInt(ldosage.getText().toString());


        //med id is declared static so it can be called using med_id within the newschedule activity
        
        dbSchedule obj=new dbSchedule(med_id,dosval,hour,minute);
        obj.save();

        List<dbSchedule> sval=dbSchedule.listAll(dbSchedule.class);
        for (int i=0;i<sval.size();i++){
            System.out.println("time"+sval.get(i).getMinute());
            System.out.println("timeh"+sval.get(i).getHour());
        }


        System.out.println("vals"+times1);
        startActivity(finish);
    }

    //calls when validation fails
    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(getActivity());


            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Snackbar snackbar=Snackbar.make(view, message, Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }
    }

}
