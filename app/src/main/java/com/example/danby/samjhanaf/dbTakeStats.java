package com.example.danby.samjhanaf;

import com.orm.SugarApp;
import com.orm.SugarRecord;

/**
 * Created by Danby on 6/13/2016.
 */
public class dbTakeStats extends SugarRecord {
    long take_id;
    long med_id;
    int time;
    int dose;

    public dbTakeStats(){

    }

    public dbTakeStats(long ti,long m,int t,int d){
        this.take_id=ti;
        this.med_id=m;
        this.time=t;
        this.dose=d;
    }



}
