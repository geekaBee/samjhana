package com.example.danby.samjhanaf;


import android.app.Activity;
import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;


/**
 * A simple {@link Fragment} subclass.
 */
public class TimePicker extends DialogFragment
        implements TimePickerDialog.OnTimeSetListener {

    public String time1;

    public TimePicker() {
        // Required empty public constructor
    }

    Calendar c = Calendar.getInstance();

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }



    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        //code for alarm
        //will be used for setting time
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);
        /* Create a new instance of TimePickerDialog and return it */
        return new TimePickerDialog(getActivity(), this, hour, minute,
                DateFormat.is24HourFormat(getActivity()));
    }


    @Override
    public void onTimeSet(android.widget.TimePicker view, int hour, int minute) {
       // DialogFragment dialogFrag = new TimePicker();
        // This is the requestCode that you are sending.
       // dialogFrag.setTargetFragment(this, 1);
        // This is the tag, "dialog" being sent.
        //dialogFrag.show(getFragmentManager(), "dialog");
        c.set(Calendar.HOUR_OF_DAY, hour);
        c.set(Calendar.MINUTE, minute);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        // set the alarm manager
        String minute1=String.valueOf(minute);
        if(minute<10){
            minute1="0"+String.valueOf(minute);
        }
        time1 = String.valueOf(hour) + ":" + minute1;
        // System.out.println("Hourvalue "+hour_string);
        TextView settime = (TextView) getActivity().findViewById(R.id.time1val);
        settime.setText(time1);
        Communicator comm = (Communicator) getTargetFragment();
        comm.respond1(c);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }
}








