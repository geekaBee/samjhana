package com.example.danby.samjhanaf;

import android.app.AlarmManager;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.support.v4.app.FragmentTransaction;
import java.util.List;


public class NewMedication extends AppCompatActivity{

    Button b;
    ImageView iv;
    public EditText medname;
    public Button nextbtn;
    public String medn;
    static long med_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_medication);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        FragmentManager manager;
        manager = getSupportFragmentManager();

        //adding the medicine name and camera image feature
        FragmentTransaction transaction = manager.beginTransaction();
        MedicationAdd medicationAdd = new MedicationAdd();
        transaction.add(R.id.group, medicationAdd, "medicationName").commit();
        b = (Button) findViewById(R.id.takepic);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 11) {
            iv = (ImageView) findViewById(R.id.imageView);
            Bitmap bp = (Bitmap) data.getExtras().get("data");
            iv.setImageBitmap(bp);
        }

    }


    //when the Next button is clicked
    public void Next(View view) {
        //add validation
        medname = (EditText)findViewById(R.id.medsname);
        nextbtn=(Button)findViewById(R.id.Next1);
        medn = medname.getText().toString();
        System.out.println("Medicine: "+medn);
        String testurl="abcd";
        dbmedstorage obj1=new dbmedstorage(medn,testurl);
        obj1.save();
        med_id=obj1.getId();
        List<dbmedstorage> wks=dbmedstorage.listAll(dbmedstorage.class);

        //code just to check database entry(need to delete pachi
        for(int i=0;i<wks.size();i++){
            System.out.println("Meds: "+wks.get(i).getMedicine());
        }
        //passing med_id to the MedsFrequency
        Bundle data=new Bundle();
        data.putLong("medicationId",med_id);


        MedsFrequency meds = new MedsFrequency();
        meds.setArguments(data);
        FragmentManager m1 = getSupportFragmentManager();
        m1.beginTransaction().replace(R.id.group, meds, "Frequency").addToBackStack(null).commit();

    }



    public void Takepic(View view) {
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, 11);

    }


    @Override
    public void onBackPressed() {

        int count = getFragmentManager().getBackStackEntryCount();

        if (count == 0) {
            super.onBackPressed();


        } else {
            getFragmentManager().popBackStack();
        }

    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}

