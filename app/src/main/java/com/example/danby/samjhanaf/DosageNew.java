package com.example.danby.samjhanaf;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class DosageNew extends Fragment {

    Spinner spinner;
    EditText medamt;
    String medselected;
    int medamout;
    int medsize;
    long idval=10;

    public DosageNew() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_dosage_new, container, false);
        // Array of choices

        List<dbmedstorage> wks = dbmedstorage.listAll(dbmedstorage.class);
        int vol = wks.size();
        String medlist[] = new String[vol];
        long medID[]=new long[vol];
        medsize=wks.size();
        for (int i = 0; i < medsize; i++) {
            medlist[i] = wks.get(i).getMedicine();
            //medID[i]=wks.get(i).getId();
            //System.out.println("MEDD"+medID[i]);
        }

        // Selection of the spinner
        spinner = (Spinner) v.findViewById(R.id.medspinner);
        Button btndnext=(Button)v.findViewById(R.id.btnnextsto);
        medamt=(EditText)v.findViewById(R.id.medaddamt);

        // Application of the Array to the Spinner
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, medlist);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
        spinner.setAdapter(spinnerArrayAdapter);

        //find which meds selected






        //add dosage to the database

        btndnext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                List<dbmedstorage> wks = dbmedstorage.listAll(dbmedstorage.class);
                medselected=String.valueOf(spinner.getSelectedItem());
                medamout=Integer.parseInt(medamt.getText().toString());
                for (int i=0;i<medsize;i++){
                    if(medselected.equals(wks.get(i).getMedicine())){
                        idval=wks.get(i).getId();
                    }
                }
                dbmedquantitystorage obj1=new dbmedquantitystorage(idval,medamout);
                obj1.save();
                //code just to check database entry(need to delete pachi
                List<dbmedquantitystorage> wk=dbmedquantitystorage.listAll(dbmedquantitystorage.class);
                for(int i=0;i<wk.size();i++){
                    System.out.println("Meds: "+wk.get(i).getMed_ID());
                }

              // System.out.println("medicinename:"+medselected+medamout);
            }
        });


        return v;
    }

}
