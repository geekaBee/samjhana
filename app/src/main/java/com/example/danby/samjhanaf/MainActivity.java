package com.example.danby.samjhanaf;

import android.app.AlarmManager;
import android.content.Intent;
import android.os.Bundle;


import android.support.design.widget.NavigationView;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;

import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.oguzdev.circularfloatingactionmenu.library.FloatingActionButton;
import com.oguzdev.circularfloatingactionmenu.library.FloatingActionMenu;
import com.oguzdev.circularfloatingactionmenu.library.SubActionButton;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {

    private static final String tag_new_med = "newMed";
    private static final String tag_new_dose = "newDose";
    private static final String tag_new_icon = "newIcon";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        FragmentTransaction tx = getSupportFragmentManager().beginTransaction();
        tx.replace(R.id.frag_main, new ScheduleDisplay());
        tx.commit();


        //Create a floating button to attach the menu
        ImageView imageView = new ImageView(this);
        imageView.setImageResource(R.drawable.ic_start);

        FloatingActionButton actionButton = new FloatingActionButton.Builder(this)
                .setContentView(imageView)
                .setBackgroundDrawable(R.drawable.selector_button)
                .build();

        //menu icon for medication
        ImageView newMedication = new ImageView(this);
        newMedication.setImageResource(R.drawable.ic_menu_camera);

        //menu icon for addition of dose
        ImageView newDose = new ImageView(this);
        newDose.setImageResource(R.drawable.ic_menu_gallery);

        //menu icon
        ImageView newIcon = new ImageView(this);
        newIcon.setImageResource(R.drawable.ic_menu_slideshow);


        //setting the subActionButton
        SubActionButton.Builder itemBuilder = new SubActionButton.Builder(this);
        //setting the size of the sub action button
        FloatingActionButton.LayoutParams params = new FloatingActionButton.LayoutParams(100, 100);
        itemBuilder.setLayoutParams(params);

        FloatingActionButton.LayoutParams params1 = new FloatingActionButton.LayoutParams(70, 70);
        itemBuilder.setLayoutParams(params);

        FloatingActionButton.LayoutParams params2 = new FloatingActionButton.LayoutParams(70, 70);
        itemBuilder.setLayoutParams(params);

        itemBuilder.setBackgroundDrawable(getResources().getDrawable(R.drawable.selector_sub_button));
        SubActionButton newMed = itemBuilder.setContentView(newMedication).build();
        newMed.setLayoutParams(params);

        SubActionButton newDos = itemBuilder.setContentView(newDose).build();
        newDos.setLayoutParams(params1);

        SubActionButton newIc = itemBuilder.setContentView(newIcon).build();
        newIc.setLayoutParams(params2);


        //setting tag to subActionButton to determine which button was clicked
        newMed.setTag(tag_new_med);
        newDose.setTag(tag_new_dose);
        newIcon.setTag(tag_new_icon);

        newMed.setOnClickListener(this);
        newDose.setOnClickListener(this);
        newIcon.setOnClickListener(this);


        FloatingActionMenu actionMenu = new FloatingActionMenu.Builder(this)
                .addSubActionView(newMed)
                .addSubActionView(newDos)
                .addSubActionView(newIc)
                .attachTo(actionButton)
                .build();


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.add_schedule) {
            ScheduleDisplay scheduleDisplay = new ScheduleDisplay();
            FragmentManager manager = getSupportFragmentManager();
            manager.beginTransaction().replace(R.id.frag_main, scheduleDisplay).commit();


        } else if (id == R.id.measurement) {
            DosageNew dosageNew = new DosageNew();
            FragmentManager manager = getSupportFragmentManager();
            manager.beginTransaction().replace(R.id.frag_main, dosageNew).commit();


        } else if (id == R.id.nav_slideshow) {
            //Toast.makeText(getApplicationContext(), "Sample Toast", Toast.LENGTH_LONG).show();
            Intent nintent = new Intent(MainActivity.this, NewMedication.class);
            nintent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(nintent);


        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        } else {
            ScheduleDisplay scheduleDisplay = new ScheduleDisplay();
            FragmentManager manager = getSupportFragmentManager();
            manager.beginTransaction().replace(R.id.frag_main, scheduleDisplay).commit();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onClick(View v) {
        final Intent intent = new Intent(MainActivity.this, NewMedication.class);
        if (v.getTag().equals(tag_new_med)) {
            startActivity(intent);

        } else if (v.getTag().equals(tag_new_dose)) {
            Toast.makeText(this, "addDose was clicked", Toast.LENGTH_LONG).show();
        } else if (v.getTag().equals(tag_new_icon)) {
            Toast.makeText(this, "addIcon was clicked", Toast.LENGTH_LONG).show();
        }
    }
}

