package com.example.danby.samjhanaf;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Danby on 6/4/2016.
 */

public class Medicine extends AppCompatActivity {

    //PendingIntent pendingIntent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.medicine);

        //received med_id from ringtonePlaying
        Long med_id = getIntent().getExtras().getLong("medId");
        Log.e("med id receive", Long.toString(med_id));

        dbmedstorage med = dbmedstorage.findById(dbmedstorage.class,med_id);

        Log.e("med name",med.medicine);

        TextView text = (TextView) findViewById(R.id.medname);
        Button button1 = (Button) findViewById(R.id.button);
        text.setText(med.medicine);
        assert button1 != null;

        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), AlarmReceiver.class);
                intent.putExtra("extra","off");
                //AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
                //alarmManager.cancel(pendingIntent);
                sendBroadcast(intent);
            }
        });


    }
}
