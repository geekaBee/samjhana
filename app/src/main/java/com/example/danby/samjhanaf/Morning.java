package com.example.danby.samjhanaf;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Danby on 5/25/2016.
 */
public class Morning extends Fragment {
    ListView l;
    String[] morning;

    List<dbmedstorage> wks=dbmedstorage.listAll(dbmedstorage.class);
    List<dbSchedule> sval=dbSchedule.listAll(dbSchedule.class);
    int vol = wks.size();
    int schsize=sval.size();
    long medID[] = new long[schsize];
    int medTimem[] = new int[schsize];
    int medTimeh[]=new int[schsize];
    String medlist[]=new String[schsize];
    String medlisty[]=new String[vol];
    String time[]=new String[schsize];
    String minvale[]=new String[schsize];
    String hourvale[]=new String[schsize];

    int[] images={R.drawable.cap4,R.drawable.cap2,R.drawable.cap3};

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v= inflater.inflate(R.layout.morning, container, false);



        for (int i=0;i<schsize;i++){
            medlisty[i] = wks.get(i).getMedicine();
            medTimeh[i]=sval.get(i).getHour();
            medTimem[i]=sval.get(i).getMinute();


            //get medID from db
            medID[i] = sval.get(i).getMed_ID();
            //convert medidval into int
            int medidval=(int)(long)medID[i];
            //find object with particular id from dbmedstorage
            dbmedstorage medname= dbmedstorage.findById(dbmedstorage.class, medidval);
            //get medicine name from dbmedstorage
            medlist[i]=medname.getMedicine();

        }
        //array holding minute value
        minvale=Arrays.toString(medTimem).split("[\\[\\]]")[1].split(", ");

        //array holding hour value
        hourvale=Arrays.toString(medTimeh).split("[\\[\\]]")[1].split(", ");
        return v;


    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);



//        System.out.println("Medsss:"+medlisty[1]);
//        System.out.println("Medsssy:"+time[1]);
        Resources res = getResources();
        morning = res.getStringArray(R.array.Morning);
        //time = res.getStringArray(R.array.time);
        l = (ListView) getView().findViewById(R.id.morningList);
        customAdapter adapter = new customAdapter(getActivity(),medlist,images,minvale,hourvale);
        l.setAdapter(adapter);
    }

}

class customAdapter extends ArrayAdapter<String>
{
    Context context;
    String[] morningArray;
    String[] hourArray;
    String[] minArray;

    int[] images;
    customAdapter(Context c,String[] medlist,int imgs[],String [] minvale,String[]hourvale){
        super(c,R.layout.single_row,R.id.textView,medlist);
        this.context=c;
        this.images=imgs;
        this.morningArray=medlist;
        this.minArray=minvale;
        this.hourArray=hourvale;


    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View row = inflater.inflate(R.layout.single_row,parent,false);
        ImageView myImage = (ImageView) row.findViewById(R.id.imageView);
        TextView medicineName = (TextView) row.findViewById(R.id.textView);
        TextView time = (TextView) row.findViewById(R.id.textView1);
        TextView minutev = (TextView) row.findViewById(R.id.minutevalue);
//        System.out.println("value: "+morningArray[1]);
        myImage.setImageResource(images[position]);
        medicineName.setText(morningArray[position]);

        //hour value set
        time.setText(hourArray[position]);

        //minute value set
        minutev.setText(minArray[position]);
        return row;

    }
}
