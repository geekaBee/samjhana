package com.example.danby.samjhanaf;

import com.orm.SugarRecord;

/**
 * Created by aayush on 6/7/16.
 */
public class dbSchedule extends SugarRecord {

    long med_id;
    int hour;
    int min;
    int amount;

    public dbSchedule() {

    }

    public dbSchedule(long m, int a,int h,int mi) {

        this.med_id= m;
        this.amount =a;
        this.hour=h;
        this.min=mi;

    }

    public void setMed_ID(long m) {

        this.med_id= m;
    }
    public long getMed_ID(){

        return med_id;
    }
    public int getAmount(){

        return amount;
    }

    public int getHour(){

        return hour;
    }
    public int getMinute(){

        return min;
    }



}


