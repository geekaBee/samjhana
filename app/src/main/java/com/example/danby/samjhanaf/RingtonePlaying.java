package com.example.danby.samjhanaf;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;

import java.util.Random;


public class RingtonePlaying extends Service {

    MediaPlayer media_song;
   // boolean isRunning;
    int start_id;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public int onStartCommand(Intent intent, int flags, int startId) {
        String state = intent.getExtras().getString("extra");
        Long med_id = intent.getExtras().getLong("medId");
        Log.e("med id in ring",Long.toString(med_id));

        assert state != null;
        if (state.equals("on")) {
            start_id = 1;
            Log.e("start id is",state);
        }

        else if(state.equals("off")){
            start_id = 0;
            Log.e("start id is",state);
        }

        if(start_id==1){
            //play a song
          media_song = MediaPlayer.create(this,R.raw.daft);
          media_song.start();
            //isRunning = true;
            start_id=0;

            //set up the notification service
            NotificationManager notify_manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

            // set up an intent that goes to the Main Activity
            Intent intent_main_activity = new Intent(this.getApplicationContext(), Medicine.class);
            intent_main_activity.putExtra("medId",med_id);
            Log.e("in ringtone",Long.toString(med_id));
            //set up a pending intent
            PendingIntent pending_intent_main_activity = PendingIntent.getActivity(this, 0, intent_main_activity, PendingIntent.FLAG_UPDATE_CURRENT);

            //make the notification parameters
            Notification notification_popup = new Notification.Builder(this)
                    .setContentTitle("An alarm is going off")
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentText("Click me!")
                    .setContentIntent(pending_intent_main_activity)
                    .setAutoCancel(true)
                    .build();


            //set up the notification call command
            notify_manager.notify(0, notification_popup);
        }

        else if(start_id==0) {
           media_song.stop();
           media_song.reset();
         //   this.isRunning = false;
            start_id = 0;
        }


        return START_NOT_STICKY;
    }
}


