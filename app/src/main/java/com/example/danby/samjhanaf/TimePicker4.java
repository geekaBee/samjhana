package com.example.danby.samjhanaf;


import android.app.Dialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.text.format.DateFormat;
import android.widget.TextView;

import java.util.Calendar;


/**
 * A simple {@link Fragment} subclass.
 */
public class TimePicker4 extends DialogFragment
        implements TimePickerDialog.OnTimeSetListener{

    public String time1;
    final Calendar c = Calendar.getInstance();

    public TimePicker4() {
        // Required empty public constructor
    }



    static TimePicker4 newInstance(){
        return new TimePicker4();
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current time as the default values for the picker
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);

        /* Create a new instance of TimePickerDialog and return it */
        return new TimePickerDialog(getActivity(), this, hour, minute,
                DateFormat.is24HourFormat(getActivity()));

    }



    @Override
    public void onTimeSet(android.widget.TimePicker view, int hour, int minute) {
        c.set(Calendar.HOUR_OF_DAY, hour);
        c.set(Calendar.MINUTE, minute);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        // set the alarm manager
        String minute1=String.valueOf(minute);
        if(minute<10){
            minute1="0"+String.valueOf(minute);
        }
        time1 = String.valueOf(hour) + ":" + minute1;
        TextView settime=(TextView)getActivity().findViewById(R.id.time4val);
        settime.setText(time1);

        Communicator comm = (Communicator) getTargetFragment();
        comm.respond4(c);


    }





}


